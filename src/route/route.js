const router = require("express").Router(); // Importa o módulo Router do Express

const userController = require('../controller/UserController')
const dbController = require("../controller/dbController"); // Importa o controlador dbController
const quadraController = require("../controller/quadraController");// Importa o controlador quadraController
const reservaController = require('../controller/reservaController');


//Rotas do quadraController
router.post('/quadra/', quadraController.postQuadra)
router.put('/quadra/', quadraController.updateQuadra)
router.delete('/quadra/:id', quadraController.deleteQuadraId)
router.get('/quadra/', quadraController.getQuadra)

//Rotas do userController
router.post('/usuarioPost/', userController.criarUsuario );
router.get('/usuarioGet/', userController.buscarUsuarios);
router.get('/usuarioGet/:id', userController.getAllReservaByID);

router.put('/AtualizarUsuario/:id', userController.atualizarUsuario);
router.delete('/DeletarUsuario/:id', userController.deletarUsuario);


// Rotas de reservaController CRUD
router.post('/reservas', reservaController.postReserva); // Rota para criar nova reserva
router.put('/reservas/:id', reservaController.updateReserva); // Rota para atualizar reserva existente
router.delete('/reservas/:id/', reservaController.deleteReserva); // Rota para deletar reserva
router.get('/reservas/:id', reservaController.getAllReservaByID); // Rota para obter reserva por ID
router.get('/reservas', reservaController.getAllReserva); // Rota para obter todas as reservas

//Rotas do dbController
router.get("/tables", dbController.getNameTables); // Rota para consultar os nomes das tabelas do banco de dados
router.get('/tablesdescription', dbController.getTablesDescription); // Rota para consultar as descrições das tabelas do banco de dados

module.exports = router; // Exporta o objeto router com as rotas definidas
