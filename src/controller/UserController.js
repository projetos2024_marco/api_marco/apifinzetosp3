const connect = require("../db/connect");

module.exports = class UsuarioController {
  static async criarUsuario(req, res) {
    const { NomeUsuario, Email, CPF, Senha } = req.body;

    if (!CPF || !Email || !Senha || !NomeUsuario) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    else if (isNaN(CPF) || CPF.length !== 11) {
      return res.status(400).json({ error: "CPF inválido. Deve conter exatamente 11 dígitos numéricos" });
    }

    else if (!Email.includes("@")) {
      return res.status(400).json({ error: "Email inválido. Deve conter @" });
    }

    else {
      const query = `INSERT INTO Usuario (NomeUsuario, Email, CPF, Senha) VALUES (?, ?, ?, ?)`;

      connect.query(query, [NomeUsuario, Email, CPF, Senha], function (err, results) {
        if (err) {
          if (err.code === 'ER_DUP_ENTRY') { 
            console.error(err);
            return res.status(400).json({ error: "CPF já cadastrado" });
          } else {
            console.error(err);
            return res.status(500).json({ error: "Erro interno do servidor" });
          }
        }
        console.log("Inserido no MySQL");
        return res.status(201).json({ message: "Usuário criado com sucesso" });
      });
    } 
  }

  static async buscarUsuarios(req, res) {
    const query = `SELECT * FROM Usuario`;

    connect.query(query, function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      return res.status(200).json({ message: "Obtendo todos os usuários", users: results });
    });
  }

 

  static async getAllReservaByID(req, res) {
    const UsuarioID = req.params.id;

    try {
        const query = 'SELECT * FROM usuario WHERE UsuarioID = ?';
        connect.query(query, [UsuarioID], function (err, result) {
            if (err) {
                console.error("Erro ao obter usuário:", err);
                return res.status(500).json({ error: "Erro interno do servidor" });
            }

            if (result.length === 0) {
                return res.status(404).json({ error: "Usuário não encontrado" });
            }

            console.log("Usuário obtido com sucesso");
            res.status(200).json({ message: "Obtendo usuário com ID: " + UsuarioID, usuario: result[0] });
        });
    } catch (error) {
        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({ error: "Erro interno do servidor" });
    }
}

static async atualizarUsuario(req, res) {
  const id = req.params.id;
  const { NomeUsuario, Email, CPF, Senha } = req.body;

  if (!CPF || !Email || !Senha || !NomeUsuario) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
  }

  // Checar se o ID é um número válido
  if (isNaN(id)) {
      return res.status(400).json({ error: "ID inválido" });
  }

  // Certifique-se de que você está passando o tipo correto de dados
  const query = 'UPDATE Usuario SET CPF = ?, Email = ?, Senha = ?, NomeUsuario = ? WHERE UsuarioID = ?';

  connect.query(query, [CPF, Email, Senha, NomeUsuario, id], function (err, results) {
      if (err) {
          console.error('Erro ao executar a query:', err);
          return res.status(500).json({ error: "Erro interno do servidor" });
      }

      if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Usuário não encontrado" });
      }

      return res.status(200).json({ message: "Usuário atualizado com ID: " + id });
  });
}

static async deletarUsuario(req, res) {
  const id = req.params.id;

  // Verificar se o ID está presente e se é um número válido
  if (!id) {
      return res.status(400).json({ error: "ID do usuário não fornecido" });
  }

  // Checar se o ID é um número válido
  if (isNaN(id)) {
      return res.status(400).json({ error: "ID inválido" });
  }

  const query = 'DELETE FROM Usuario WHERE UsuarioID = ?';

  connect.query(query, [id], function (err, results) {
      if (err) {
          console.error('Erro ao executar a query:', err);
          return res.status(500).json({ error: "Erro interno do servidor" });
      }

      if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Usuário não encontrado" });
      }

      return res.status(200).json({ message: "Usuário excluído com ID: " + id });
  });
}

  /*static async postLogin(req, res) {
    const { Email, Senha } = req.body;

    if (!Email || !Senha) {
      return res.status(400).json({ error: "Email e Senha são obrigatórios" });
    }

    const query = `SELECT * FROM user WHERE Email = ? AND Senha = ?`;

    connect.query(query, [Email, Senha], function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      if (results.length === 0) {
        return res.status(401).json({ error: "Credenciais inválidas" });
      }

      return res.status(200).json({ message: "Login realizado com sucesso", user: results[0] });
    });
  }*/

  static async buscarUsuariobyId(req, res) {
    const id = req.params.id;
    const query = `SELECT * FROM Usuario WHERE CPF = ?`;

    connect.query(query, [id], function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      if (results.length === 0) {
        return res.status(404).json({ error: "Usuário não encontrado" });
      }

      return res.status(200).json({ message: "Obtendo usuário com ID: " + id, user: results[0] });
    });
  }
};
