const connect = require("../db/connect");

class ReservaController {
  static async postReserva(req, res) {
    const { QuadraID, TipoQuadra, UsuarioID, ValorPeriodo, Disponibilidade, Periodo } = req.body;

    // Verifica se todos os campos estão preenchidos
    if (!QuadraID || !TipoQuadra || !UsuarioID || !ValorPeriodo || !Disponibilidade || !Periodo) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Validação dos valores permitidos para o campo Periodo
    const periodosPermitidos = ["Manhã", "Tarde", "Noite"];
    if (!periodosPermitidos.includes(Periodo)) {
      return res.status(400).json({ error: "Periodo deve ser um dos seguintes: Manhã, Tarde, Noite" });
    }

    const query = `INSERT INTO reservas (QuadraID, TipoQuadra, UsuarioID, ValorPeriodo, Disponibilidade, Periodo) VALUES (?, ?, ?, ?, ?, ?)`;

    try {
      connect.query(query, [QuadraID, TipoQuadra, UsuarioID, ValorPeriodo, Disponibilidade, Periodo], function (err) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Erro ao cadastrar reserva" });
          return;
        }
        console.log("Reserva cadastrada com sucesso");
        res.status(201).json({ message: "Reserva cadastrada com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getAllReserva(req, res) {
    try {
      const query = "SELECT * FROM reservas";
      connect.query(query, function (err, result) {
        if (err) {
          console.error("Erro ao obter reservas:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        console.log("Reservas obtidas com sucesso");
        res.status(200).json({ message: "Obtendo todas as reservas", reservas: result });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getAllReservaByID(req, res) {
    const ReservaID = req.params.id;

    try {
      const query = `SELECT * FROM reservas WHERE ReservaID = '${ReservaID}'`;
      connect.query(query, function (err, result) {
        if (err) {
          console.error("Erro ao obter reserva:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        if (result.length === 0) {
          return res.status(404).json({ error: "Reserva não encontrada" });
        }

        console.log("Reserva obtida com sucesso");
        res.status(200).json({ message: "Obtendo a reserva com ID: " + ReservaID, reserva: result[0] });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async updateReserva(req, res) {
    const ReservaID = req.params.id;
    const { QuadraID, TipoQuadra, UsuarioID, ValorPeriodo, Disponibilidade, Periodo } = req.body;

    if (!QuadraID || !TipoQuadra || !UsuarioID || !ValorPeriodo || !Disponibilidade || !Periodo) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    const periodosPermitidos = ["Manhã", "Tarde", "Noite"];
    if (!periodosPermitidos.includes(Periodo)) {
      return res.status(400).json({ error: "Periodo deve ser: Manhã, Tarde, Noite" });
    }

    const query = `UPDATE reservas SET QuadraID = '${QuadraID}', TipoQuadra = '${TipoQuadra}', UsuarioID = '${UsuarioID}', ValorPeriodo = '${ValorPeriodo}', Disponibilidade = '${Disponibilidade}', Periodo = '${Periodo}' WHERE ReservaID = '${ReservaID}'`;

    try {
      connect.query(query, function (err) {
        if (err) {
          console.error("Erro ao atualizar reserva:", err);
          res.status(500).json({ error: "Erro interno do servidor" });
          return;
        }
        console.log("Reserva atualizada com sucesso");
        res.status(200).json({ message: "Reserva atualizada com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async deleteReserva(req, res) {
    const ReservaID = req.params.id;

    const query = `DELETE FROM reservas WHERE ReservaID = '${ReservaID}'`;

    try {
      connect.query(query, function (err) {
        if (err) {
          console.error("Erro ao deletar reserva:", err);
          res.status(500).json({ error: "Erro interno do servidor" });
          return;
        }
        console.log("Reserva deletada com sucesso");
        res.status(200).json({ message: "Reserva deletada com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
}

module.exports = ReservaController;

